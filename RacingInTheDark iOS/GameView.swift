//
//  GameView.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/7/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

func ridClamp(value: Float, _ min: Float, _ max: Float) -> Float {
    if value < min {
        return min
    }
    
    if value > max {
        return max
    }
    
    return value
}

class GameView: SCNView {
    
    var _inputHandler: RIDInputHandler!
    var _overlay: protocol<RIDButtonOverlay, RIDOverlayMenuView>!
    
    var throttleTouch: UITouch?
    var throttleTouchDown: CGFloat = 0
    var steeringTouch: UITouch?
    var steeringTouchDown: CGFloat = 0

    var throttleButton: UITouch?
    var breakButton: UITouch?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let p = touch.locationInNode(_overlay.rootNode)

//            trilog.i("began \(p)")

            switch _inputHandler.inputMode {
            case .Game:
                if let button = _overlay.buttonAtPoint(p) {
                    switch (button) {
                    case .Throttle:
                        _inputHandler.gameInputController?.pressKey(.Throttle)
                        throttleButton = touch
                        break
                    case .Break:
                        _inputHandler.gameInputController?.pressKey(.Break)
                        breakButton = touch
                        break
                    default:
                        break
                    }
                }

                if touch.tapCount > 3 {
                    // FIXME
                    //                playerControl.resetToStart = true
                }
                if touch.locationInView(self).x > frame.midX {
                    //                throttleTouch = touch
                    //                throttleTouchDown = touch.locationInView(self).y
                } else {
                    steeringTouch = touch
                    steeringTouchDown = touch.locationInView(self).x
                }
            break
            case .Menu:
                if let index = _overlay.menuItemIndexAtPoint(touch.locationInNode(_overlay.rootNode)) {
                    _inputHandler.menuInputController.activateMenuItemAtIndex(index)
                }
                break
            case .None:
                break
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {

//            trilog.i("move \(touch)")

            if touch === throttleTouch {
//                let delta = throttleTouchDown - touch.locationInView(self).y
//                let throttle = clamp(delta / 100, -1.0, 1.0)
//                if throttle >= 0 {
//                    playerControl.throttle = throttle
//                    playerControl.brake = 0
//                } else {
//                    playerControl.throttle = 0
//                    playerControl.brake = -throttle
//                }
            } else if touch === steeringTouch {
                let delta = steeringTouchDown - touch.locationInView(self).x
                _inputHandler.gameInputController?.steering = ridClamp(Float(delta) / Float(150), -1.0, 1.0)
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {

            if touch === throttleButton {
                _inputHandler.gameInputController?.releaseKey(.Throttle)
            } else if touch === breakButton {
                _inputHandler.gameInputController?.releaseKey(.Break)
            }

            if touch === throttleTouch {
//                throttleTouch = nil
//                playerControl.releaseThrottleKey()
//                playerControl.releaseBrakeKey()
            } else if touch === steeringTouch {
                steeringTouch = nil 
                _inputHandler.gameInputController?.steering = 0
            }
        }
    }
}
