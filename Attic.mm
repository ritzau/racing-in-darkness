//
//  Attic.m
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/24/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#include <iomanip>
#include <iostream>
#include <memory>

#include <btBulletDynamicsCommon.h>

using namespace std;

extern "C"
void foo() {
    
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
    auto collisionConfiguration = make_unique<btDefaultCollisionConfiguration>();
    
    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    auto dispatcher = make_unique<btCollisionDispatcher>(collisionConfiguration.get());
    
    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    auto overlappingPairCache = make_unique<btDbvtBroadphase>();
    
    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    auto solver = make_unique<btSequentialImpulseConstraintSolver>();
    
    auto dynamicsWorld = make_unique<btDiscreteDynamicsWorld>(dispatcher.get(),
                                                              overlappingPairCache.get(),
                                                              solver.get(),
                                                              collisionConfiguration.get());
    
    dynamicsWorld->setGravity(btVector3{0, -10, 0});
    
    auto motionState = new btDefaultMotionState{};
    auto shape = new btSphereShape{1.0};
    auto body = new btRigidBody{1.0, motionState, shape};
    
    dynamicsWorld->addRigidBody(body);
    
    cout << fixed;
    
    for (auto i = 0; i < 100; ++i) {
        dynamicsWorld->stepSimulation(1.f/60.f, 10);
        
        //print positions of all objects
        for (auto j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0 ; --j) {
            auto obj = dynamicsWorld->getCollisionObjectArray()[j];
            auto body = btRigidBody::upcast(obj);
            
            btTransform trans;
            if (body && body->getMotionState()) {
                body->getMotionState()->getWorldTransform(trans);
            } else {
                trans = obj->getWorldTransform();
            }
            
            cout << "world pos object " << j << " = "
            << setw(7) << setprecision(3) << trans.getOrigin().getX() << ", "
            << setw(7) << setprecision(3) << trans.getOrigin().getY() << ", "
            << setw(7) << setprecision(3) << trans.getOrigin().getZ() << endl;
        }
    }
    
    //remove the rigidbodies from the dynamics world and delete them
    for (auto i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0 ; --i) {
        auto obj = dynamicsWorld->getCollisionObjectArray()[i];
        auto body = btRigidBody::upcast(obj);
        
        if (body && body->getMotionState()) {
            delete body->getMotionState();
        }
        
        dynamicsWorld->removeCollisionObject(obj);
        
        delete obj;
    }
    
    // delete
    //    for (int j=0;j<collisionShapes.size();j++)
    //    {
    //        btCollisionShape* shape = collisionShapes[j];
    //        collisionShapes[j] = 0;
    //        delete shape;
    //    }
    
    //next line is optional: it will be cleared by the destructor when the array goes out of scope
    //    collisionShapes.clear();
}



