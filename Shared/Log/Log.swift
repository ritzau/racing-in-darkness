//
//  Log.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/29/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

let trilog = TRILog.get("#")

extension TRILog {
    static func get(_ tag: NSString = #file) -> TRILog {
        return TRILog(tag: tag.lastPathComponent)
    }
}

