//
//  Log.h
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/28/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#pragma once

#include <Foundation/Foundation.h>

#ifndef TRILOG_STRIP
#define TRILOG_STRIP TRILogLevelMin
#endif

typedef NS_ENUM(NSInteger, TRILogLevel) {
    TRILogLevelMin,
    TRILogLevelVerbose,
    TRILogLevelDebug,
    TRILogLevelInfo,
    TRILogLevelWarning,
    TRILogLevelError,
    TRILogLevelMax,
};

const NSInteger TRILogStripLevel = TRILOG_STRIP;

@interface TRILog : NSObject

@property (readonly) NSString* tag;
@property TRILogLevel level;

- (id) initWithTag: (NSString*) tag;

- (bool) isLoggable: (TRILogLevel) level;

- (void) log: (TRILogLevel) level format: (NSString*) format arguments: (va_list) arguments;

//- (void) log: (TRILogLevel) level format: (NSString*) format, ...;

- (void) vfmt: (NSString*) format, ...;

- (void) v: (NSString*) message;

- (void) dfmt: (NSString*) format, ...;

- (void) d: (NSString*) message;

- (void) ifmt: (NSString*) format, ...;

- (void) i: (NSString*) message;

- (void) wfmt: (NSString*) format, ...;

- (void) w: (NSString*) message;

- (void) efmt: (NSString*) format, ...;

- (void) e: (NSString*) message;

@end