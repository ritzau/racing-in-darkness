//
//  Log.m
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/28/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#import "Log.h"

static NSArray* _triLogTagNames;

static NSString* _triLogTagName(long level) {
    if (!_triLogTagNames) {
        _triLogTagNames = @[@"M", @"V", @"D", @"I", @"W", @"E", @"X"];
    }
    return _triLogTagNames[level];
}

@implementation TRILog {
    NSString* _tag;
}

- (id) initWithTag: (NSString*) tag {
    self = [super init];

    if (self) {
        _tag = tag;
        self.level = TRILogLevelDebug;
    }

    return self;
}

- (NSString*) tag {
    return _tag;
}

- (bool) isLoggable: (TRILogLevel) level {
    return level > TRILogStripLevel && self.level <= level;
}

- (void) log: (TRILogLevel) level format: (NSString*) format arguments: (va_list) arguments {
    if ([self isLoggable: level]) {
        NSString* formatted = [[NSString alloc] initWithFormat: format arguments: arguments];
        NSLog(@"%@(%@): %@", _tag, _triLogTagName((long) level), formatted);
    }
}

//- (void) log: (TRILogLevel) level message: (NSString*) format, ... {
//    va_list argumentList;
//    va_start(argumentList, format);
//    [self log: TRILogLevelVerbose format: format arguments: argumentList];
//    va_end(argumentList);
//}

- (void) vfmt: (NSString*) message, ... {
    va_list argumentList;
    va_start(argumentList, message);
    [self log: TRILogLevelVerbose format: message arguments: argumentList];
    va_end(argumentList);
}

- (void) dfmt: (NSString*) message, ... {
    va_list argumentList;
    va_start(argumentList, message);
    [self log: TRILogLevelDebug format: message arguments: argumentList];
    va_end(argumentList);
}


- (void) ifmt: (NSString*) message, ... {
    va_list argumentList;
    va_start(argumentList, message);
    [self log: TRILogLevelInfo format: message arguments: argumentList];
    va_end(argumentList);
}


- (void) wfmt: (NSString*) message, ... {
    va_list argumentList;
    va_start(argumentList, message);
    [self log: TRILogLevelWarning format: message arguments: argumentList];
    va_end(argumentList);
}


- (void) efmt: (NSString*) message, ... {
    va_list argumentList;
    va_start(argumentList, message);
    [self log: TRILogLevelError format: message arguments: argumentList];
    va_end(argumentList);
}

- (void) v: (NSString*) message {
    [self vfmt: @"%@", message];
}

- (void) d: (NSString*) message {
    [self dfmt: @"%@", message];
}

- (void) i: (NSString*) message {
    [self ifmt: @"%@", message];
}

- (void) w: (NSString*) message {
    [self wfmt: @"%@", message];
}

- (void) e: (NSString*) message {
    [self efmt: @"%@", message];
}


@end