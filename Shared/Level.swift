//
//  Level.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/24/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation
import SceneKit

class RIDGameImpl: RIDGame {
    var player: RIDPlayer
    let controls: RIDGameInputController = RIDGameInputControllerImpl()
    var level: RIDLevel

    init(scene: SCNScene, overlay: RIDLevelOverlay) {
        player = RIDPlayerImpl(controls: controls)
        level = RIDLevel(
            scene: scene,
            controls: controls,
            overlayUpdater: RIDOverlayUpdaterImpl(overlay: overlay))
    }

    var carCamera: SCNNode? {
        return level.scene.rootNode.childNode(withName: RDCamera.Car.rawValue, recursively: true)
    }
    
    func start() {
        level.start(&player)
    }
}

protocol RIDLevelOverlay: class {
    var raceStopWatch: TimeInterval { get set }
    var lapStopWatch: TimeInterval { get set }
    var velocity: Float { get set }
}

protocol RIDOverlayUpdater {
    func updateHeadsUpDisplay(_ player: RIDPlayer)
}

struct RIDOverlayUpdaterImpl: RIDOverlayUpdater {
    let overlay: RIDLevelOverlay

    func updateHeadsUpDisplay(_ player: RIDPlayer) {
        overlay.raceStopWatch = player.elapsedRacingTime
        overlay.lapStopWatch = player.elapsedRacingTime
        overlay.velocity = player.velocity
    }
}

struct RIDLevel {
    fileprivate let _scenekit: RIDSceneKitUniverse
    fileprivate let _physics: RIDBulletUniverse
    fileprivate let _universe: RIDUniverse
    fileprivate let _overlayUpdater: RIDOverlayUpdater

    fileprivate var scene: SCNScene {
        return _scenekit.scene
    }

    init(scene: SCNScene, controls: RIDGameInputController, overlayUpdater: RIDOverlayUpdater) {
        _physics = RIDBulletUniverse(playerControls: controls)
        _scenekit = RIDSceneKitUniverse(scene: scene)
        _universe = RIDUniverse(physics: _physics, graphics: _scenekit)
        _overlayUpdater = overlayUpdater

        buildLevel()
    }

    func start(_ player: inout RIDPlayer) {
        player.startRace()
        
        _scenekit.registerPhysicsCallback {
            [player] in
            self._overlayUpdater.updateHeadsUpDisplay(player)
            self._universe.step(1.0/60.0)
        }
    }

    // FIXME factor out
    fileprivate func buildLevel() {
        _universe.createBall()
        _universe.createBall()
        _universe.createBall()

        _universe.createVehicleAt(SCNVector3(0, 3, 4))

        var renderer = RIDTrackRenderer(resolution: RIDTrackResolution)
        for segment in RIDSelectedTrack.segments {
            _universe.createTrackSegmentAt(segment, renderer: &renderer)
        }

        if RIDSelectedTrack.closeTheLoop {
            do {
                for segment in try renderer.closeTheLoop() {
                    _universe.createTrackSegmentAt(segment, renderer: &renderer)
                }
            } catch {
                // FIXME
                print("sket sej")
            }
        }

        if RIDEnableFloor {
            _universe.createFloor(-0.01)
        }
    }

}
