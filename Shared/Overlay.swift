//
//  LovelOverlay.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/11/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation
import SpriteKit

enum RDOverlayButton {
    case throttle
    case brake
}

class RIDOverlayImpl: RIDOverlay {
    let margin: CGFloat = 20

    let scene: SKScene
    var root: SKNode
    var menu: RIDMenu?
    var menuDelegate: RIDMenuViewDelegate
    let menuView: RIDOverlayMenuView

    let timeLabelNode = SKLabelNode(fontNamed: "ChalkDuster")
    let lapTimeLabelNode = SKLabelNode(fontNamed: "ChalkDuster")
    let speedLabelNode = SKLabelNode(fontNamed: "ChalkDuster")

    var brakeButtonNode: SKSpriteNode!
    var throttleButtonNode: SKSpriteNode!
    fileprivate let _buttonDictionary: [SKNode: RIDGameInputKey]

    init(overlayScene: SKScene, hasTouch: Bool) {
        self.scene = overlayScene

        timeLabelNode.text = "Banana!"
        timeLabelNode.fontColor = RDColor.yellow
        timeLabelNode.horizontalAlignmentMode = .left
        timeLabelNode.verticalAlignmentMode = .top
        timeLabelNode.position = CGPoint(x: margin, y: scene.size.height - margin)

        lapTimeLabelNode.text = "Banana!"
        lapTimeLabelNode.fontColor = RDColor.yellow
        lapTimeLabelNode.horizontalAlignmentMode = .right
        lapTimeLabelNode.verticalAlignmentMode = .top
        lapTimeLabelNode.position = CGPoint(x: scene.size.width - margin, y: scene.size.height - margin)
        
        speedLabelNode.text = "Banana!"
        speedLabelNode.fontColor = RDColor.red
        speedLabelNode.horizontalAlignmentMode = .left
        speedLabelNode.verticalAlignmentMode = .top
        speedLabelNode.position = CGPoint(x: margin, y: timeLabelNode.frame.origin.y)

        brakeButtonNode = SKSpriteNode(imageNamed: "bokeh.png")
        throttleButtonNode = SKSpriteNode(imageNamed: "bokeh.png")
        
        let sz: CGFloat = 100
        brakeButtonNode.size = CGSize(width: sz, height: sz)
        brakeButtonNode.position = CGPoint(x: scene.frame.maxX - sz, y: scene.frame.minY + 2 * sz + 4 * margin)
        throttleButtonNode.size = CGSize(width: sz, height: sz)
        throttleButtonNode.position = CGPoint(x: scene.frame.maxX - sz, y: scene.frame.minY + sz + 2 * margin)
        
        root = SKNode()
        root.addChild(timeLabelNode)
        root.addChild(speedLabelNode)
//        root.addChild(lapTimeLabelNode) // FIXME

        if hasTouch {
            root.addChild(brakeButtonNode)
            root.addChild(throttleButtonNode)
        }

        let menuNode = SKNode()
        menuNode.name = "menu"
        menuNode.position = CGPoint(x: 200, y: 200)
        let overlayMenu = RIDSpriteKitOverlayMenu(parentNode: menuNode)
        menuDelegate = overlayMenu
        menuView = overlayMenu
        menu = buildSampleMenu(menuNode)
        menu!.viewDelegate = menuDelegate
        menuDelegate.didActivateMenu(menu!.currentMenu) // FIXME Automate
        root.addChild(menuNode)

        scene.addChild(root)

        _buttonDictionary = [
            brakeButtonNode: .break,
            throttleButtonNode: .throttle,
        ]

//        for var y = CGFloat(0); y < overlayScene.size.height/2; y += 50 {
//            for var x = CGFloat(0); x < overlayScene.size.width/2; x += 50 {
//                let node = SKLabelNode()
//                node.name = "\(Int(x)):\(Int(y))"
//                node.text = "\(Int(x)):\(Int(y))"
//                node.fontSize = 12
//                node.position = CGPoint(x: x + 25, y: y + 25)
//                node.verticalAlignmentMode = .Center
//                node.horizontalAlignmentMode = .Center
//                root.addChild(node)
//
//                let backgroundNode = SKSpriteNode()
//                backgroundNode.position = CGPoint(x: x + 5, y: y + 5)
//                backgroundNode.size = CGSize(width: 40, height: 40)
//                backgroundNode.color = RDColor.yellowColor()
//                backgroundNode.name = "bg \(Int(x)):\(Int(y))"
//                root.addChild(backgroundNode)
//            }
//        }
    }

    var rootNode: SKNode { return scene }

    var velocity: Float = 0 {
        didSet {
            updateSpeed(velocity)
        }
    }

    var raceStopWatch: TimeInterval = 0 {
        didSet {
            updateRaceTime(raceStopWatch)
        }
    }

    var lapStopWatch: TimeInterval = 0 {
        didSet {
            updateLapTime(lapStopWatch)
        }
    }

    var isPresentingMenu: Bool {
        guard let menuNode = root.childNode(withName: "menu") else {
            preconditionFailure("No menu node")
        }

        return !menuNode.isHidden
    }

    func showMenu() {
        guard let menuNode = root.childNode(withName: "menu") else {
            preconditionFailure("No menu node")
        }

        // FIXME return to root?
        menuNode.isHidden = false
    }
    
    func hideMenu() {
        guard let menuNode = root.childNode(withName: "menu") else {
            preconditionFailure("No menu node")
        }

        menuNode.isHidden = true
    }
    
    func buttonAtPoint(_ point: CGPoint) -> RIDGameInputKey? {
        return _buttonDictionary[root.atPoint(point)]
    }

    func menuItemIndexAtPoint(_ point: CGPoint) -> Int? {
        print("hit: \(scene.nodes(at: point).map { $0.name })")
        return menuView.menuItemIndexAtPoint(point)
    }

    func updateRaceTime(_ time: TimeInterval) {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm:ss.SSS"
        timeLabelNode.text = formatter.string(from: Date(timeIntervalSinceReferenceDate: time))
    }
    
    func updateLapTime(_ time: TimeInterval) {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm:ss.SSS"
        lapTimeLabelNode.text = formatter.string(from: Date(timeIntervalSinceReferenceDate: time))
    }
    
    func updateSpeed(_ speed: Float) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfEven
        formatter.roundingIncrement = 1
        speedLabelNode.text = "\(formatter.string(from: NSNumber(value: speed))!) km/h"
    }
}
