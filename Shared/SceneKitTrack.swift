//
//  NewSceneKitTrack.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 12/10/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

func renderSegment(_ segmentId: Int, vertices: [(left: SCNVector3, center: SCNVector3, right: SCNVector3)]) -> SCNNode {
    func createTrackNode() -> SCNNode {
        let node = SCNNode()
        node.categoryBitMask = CATEGORY_TRACK

        return node
    }

    func createTrackMaterial(_ color: RDColor) -> SCNMaterial {
        let material = SCNMaterial()
        material.diffuse.contents = color

        return material
    }

    func createDotAtPosition(_ pos: SCNVector3, radius: RDFloat, color: RDColor) -> SCNNode {
        let material = SCNMaterial()
        material.diffuse.contents = color

        let node = SCNNode()
        node.categoryBitMask = CATEGORY_TRACK
        node.geometry = SCNSphere(radius: CGFloat(radius))
        node.geometry!.materials = [material]
        node.position = pos

        return node
    }

    func trackColor(_ segmentId: Int) -> RDColor {
        return segmentId % 2 == 0 ?
            RDColor(red: 1, green: 0, blue: 0, alpha: 0.2) :
            RDColor(red: 0, green: 0, blue: 1, alpha: 0.2)
    }

    let node = createTrackNode()

//    let coords = vertices.flatMap { [$0.left, $0.right] }
//    let indices = Array<CInt>(coords.indices.map{CInt($0)})
//    node.geometry = SCNGeometry(
//        sources: [SCNGeometrySource(vertices: coords, count: coords.count)],
//        elements: [SCNGeometryElement(indices: indices, primitiveType: .TriangleStrip)])
//    node.geometry!.materials = [createTrackMaterial(trackColor(segmentId))]

    let ballColor = (segmentId % 2 == 0 ?
        RDColor.red :
        RDColor.blue) // FIXME .highlightWithLevel(t)!
    
    for cs in vertices[1..<vertices.count] {
        node.addChildNode(createDotAtPosition(cs.left, radius: 0.2, color: ballColor))
        node.addChildNode(createDotAtPosition(cs.center, radius: 0.05, color: ballColor))
        node.addChildNode(createDotAtPosition(cs.right, radius: 0.2, color: ballColor))
    }

    return node
}
