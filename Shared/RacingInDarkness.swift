//
//  RacingInDarkness.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 11/30/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import SpriteKit

enum RIDInputMode {
    case none, menu, game
}

protocol RIDInputHandler {
    var inputMode: RIDInputMode { get }
    var menuInputController: RIDMenuInputController { get }
    var gameInputController: RIDGameInputController? { get }
}

enum RIDMenuInputKey {
    case up, down, back, enter
}

enum RIDGameInputKey {
    case throttle,
         `break`,
         handbreak,
         steerLeft,
         steerRight,
         reset
}

protocol RIDMenuInputController: class {
    func pressKey(_ key: RIDMenuInputKey)
    func activateMenuItemAtIndex(_ index: Int)
}

protocol RIDGameInputController: class {
    func pressKey(_ key: RIDGameInputKey)
    func releaseKey(_ key: RIDGameInputKey)

    var throttle: Float { get set }
    var breaking: Float { get set }
    var steering: Float { get set }
}

protocol RIDUser {
    var name: String { get }
}

protocol RIDPlayer {
    var user: RIDUser? { get }
    var controls: RIDGameInputController { get }
    var elapsedRacingTime: TimeInterval { get }
    var velocity: Float { get }

    func startRace()
}

protocol RIDGame {
    var player: RIDPlayer { get }
    var controls: RIDGameInputController { get }
    var carCamera: SCNNode? { get }
    func start()
}

protocol RIDMenu: class {
    var viewDelegate: RIDMenuViewDelegate? { get set }
    var menuDelegate: RIDMenuDelegate? { get set }
    var currentMenu: RIDSubmenuItem { get }
    
    func selectPrevious()
    func selectNext()
    func back()
    func activateSelectedItem()
    func activateItemAtIndex(_ index: Int)
}

protocol RIDButtonOverlay {
    var rootNode: SKNode { get } // FIXME Get rid of SpriteKit dep here
    func buttonAtPoint(_ point: CGPoint) -> RIDGameInputKey? // FIXME not only game keys?
}

protocol RIDOverlay: class, RIDButtonOverlay, RIDOverlayMenuView, RIDLevelOverlay {
    var menu: RIDMenu? { get }
    var isPresentingMenu: Bool { get }

    func showMenu()
    func hideMenu()
}

protocol RIDRacingInDarkness: RIDInputHandler {
    var user: RIDUser? { get }
    var overlay: RIDOverlay { get }
}
