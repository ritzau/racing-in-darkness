//
//  AlternativeTrackModel.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/9/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit // For Vectors and Matrices
import CoreGraphics

typealias TransferFunction = (RDFloat) -> RDFloat

func linear(_ to: RDFloat) -> TransferFunction {
    return linear(0, to)
}

func linear(_ from: RDFloat, _ to: RDFloat) -> TransferFunction {
    return {from + $0 * (to - from)}
}

func cubicBezier(_ to: RDFloat, bezier: UnitBezier) -> TransferFunction {
    return cubicBezier(0, to, bezier: bezier)
}

func cubicBezier(_ from: RDFloat, _ to: RDFloat, bezier: UnitBezier) -> TransferFunction {
    return {from + bezier.solve($0, epsilon: 1e-6) * (to - from)}
}

func ease(_ to: RDFloat) -> TransferFunction {
    return ease(0, to)
}

func ease(_ from: RDFloat, _ to: RDFloat) -> TransferFunction {
    return cubicBezier(from, to, bezier: UnitBezier.ease())
}

func const(_ value: RDFloat) -> TransferFunction {
    return linear(value, value)
}

protocol RIDTrackSegment {
    var length: RDFloat { get }
    func x(_ t: RDFloat) -> RDFloat
    func y(_ t: RDFloat) -> RDFloat
    func z(_ t: RDFloat) -> RDFloat
    func roll(_ t: RDFloat) -> RDFloat
    func width(_ t: RDFloat) -> RDFloat?
    func xzDirection(_ t: RDFloat) -> SCNVector3?
}


struct RDLineSegment: RIDTrackSegment {
    fileprivate let dx: TransferFunction?
    fileprivate let dy: TransferFunction?
    fileprivate let dz: TransferFunction?
    fileprivate let roll: TransferFunction?
    fileprivate let widthFunction: TransferFunction?
    
    init(dz: RDFloat, dx: TransferFunction? = nil, dy: TransferFunction? = nil, roll: TransferFunction? = nil, width: TransferFunction? = nil) {
        self.dx = dx
        self.dy = dy
        self.dz = linear(dz)
        self.roll = roll
        self.widthFunction = width
    }
    
    var length: RDFloat {
        let lx = dx?(1) ?? 0
        let ly = dy?(1) ?? 0
        let lz = dz?(1) ?? 0
        
        return sqrt(lx*lx + ly*ly + lz*lz)
    }
    
    func x(_ t: RDFloat) -> RDFloat {
        return dx?(t) ?? 0
    }
    
    func y(_ t: RDFloat) -> RDFloat {
        return dy?(t) ?? 0
    }
    
    func z(_ t: RDFloat) -> RDFloat {
        return dz?(t) ?? 0
    }
    
    func roll(_ t: RDFloat) -> RDFloat {
        return roll?(t) ?? 0
    }
    
    func width(_ t: RDFloat) -> RDFloat? {
        return widthFunction?(t)
    }

    func xzDirection(_ t: RDFloat) -> SCNVector3? {
        return nil
    }
}

struct RDTurnSegment: RIDTrackSegment {
    fileprivate let radians: RDFloat
    fileprivate let radius: RDFloat
    fileprivate let roll: TransferFunction?
    fileprivate let width: TransferFunction?

    init(turns: RDFloat, radius: RDFloat, roll: TransferFunction? = nil, width: TransferFunction? = nil) {
        self.radians = turns * RDFloat(2 * M_PI)
        self.radius = radius
        self.roll = roll
        self.width = width
    }
    
    var length: RDFloat {
        return abs(radius * radians)
    }
    
    func x(_ t: RDFloat) -> RDFloat {
        let value = radius * (cos(t * radians) - 1)
        return radians > 0 ? value : -value
    }
    
    func y(_ t: RDFloat) -> RDFloat {
        return 0
    }
    
    func z(_ t: RDFloat) -> RDFloat {
        return radius * sin(t * abs(radians))
    }
    
    func roll(_ t: RDFloat) -> RDFloat {
        return roll?(t) ?? 0
    }
    
    func width(_ t: RDFloat) -> RDFloat? {
        return width?(t)
    }

    func xzDirection(_ t: RDFloat) -> SCNVector3? {
//        trilog.e("direction=\(SCNVector3(-sin(t * radians), 0, cos(t * radians)))")
        return SCNVector3(-sin(t * radians), 0, cos(t * radians))
    }
}


struct RIDTrack {
    let segments: [RIDTrackSegment]
    let closeTheLoop: Bool
}


enum RDTrackError: Error {
    case cantCloseLoop
}

struct RIDTrackRenderer {
    
    fileprivate let resolution: RDFloat
    fileprivate var lastWidth: RDFloat?
    fileprivate var worldTransform: SCNMatrix4
    fileprivate var lastPosition = SCNVector3(0, 0, -1)
    fileprivate var lastDelta = SCNVector3()

    init(resolution: RDFloat) {
        self.resolution = resolution
        self.worldTransform = SCNMatrix4MakeTranslation(0, 0, 0)
    }
    
    mutating func renderSegment(_ segment: RIDTrackSegment) -> [(left: SCNVector3, center: SCNVector3, right: SCNVector3)] {
        let step = 1.0 / resolution
        let count = Int(resolution)
        
        var coords: [(left: SCNVector3, center: SCNVector3, right: SCNVector3)] = []

        lastPosition = SCNVector3(0, 0, -1)
        lastDelta = SCNVector3()

        var t: RDFloat = 0
        
        for _ in 0...count {
            let pos = SCNVector3(segment.x(t), segment.y(t), segment.z(t))
//            trilog.e("edirectio=\((pos - lastPosition))")
            let perp = perpendicularXZUnitVector(segment.xzDirection(t) ?? (pos - lastPosition))
            let m = SCNMatrix4MakeRotation(
                segment.roll(t) * CG_2PI,
                pos.x - lastPosition.x,
                pos.y - lastPosition.y,
                pos.z - lastPosition.z)
            let rot = m * perp

            guard let width = segment.width(t) ?? lastWidth else {
                preconditionFailure("Define the width of the first track segment")
            }

            let left = pos + width/2 * rot
            let right = pos - width/2 * rot

            //            trilog.i("t=\(worldTransform)")
            //            trilog.i("p=\(pos) wp=\(worldTransform * pos)")
            coords.append((worldTransform * left, worldTransform * pos, worldTransform * right))
            //            trilog.i("\(coords.last!.center)")
            lastWidth = width

            lastDelta = pos - lastPosition
            lastPosition = pos
            
            t += step
        }
        // FIXME world transform?
        if let delta = segment.xzDirection(1.0) {
            lastDelta = delta
        }
        let v1 = worldTransform * SCNVector3(0, 0, 0)
        let v2 = worldTransform * lastPosition
        let v = v2 - v1

        worldTransform = SCNMatrix4Translate(worldTransform, v.x, v.y, v.z)
        let rotation = SCNMatrix4MakeRotation(atan2(lastDelta.x, lastDelta.z), 0, 1, 0)
        worldTransform = SCNMatrix4Mult(rotation, worldTransform)

        lastPosition = coords.last!.center
        var worldRotation = worldTransform
        worldRotation.m41 = 0
        worldRotation.m42 = 0
        worldRotation.m43 = 0
        lastDelta = worldRotation * SCNVector3Make(0, 0, 1)

//        print("\(worldRotation.m11)"
        print("lastPos=\(lastPosition) d=\(lastDelta)")

        return coords
    }


    private func closeLoop(_ p1: SCNVector3, _ v1: SCNVector3) throws ->
        (dy: RDFloat, length0: RDFloat, length1: RDFloat, radius: RDFloat, alpha: RDFloat)
    {
        var p1 = p1
        let p0 = SCNVector3(0, 0, 0)
        let v0 = SCNVector3(0, 0, -1)

        let dy = p0.y - p1.y + 0.001
        p1.y = 0

        func solveCrossSection(_ p1: SCNVector3, _ v1: SCNVector3) throws -> SCNVector3 {
            guard v1.x != 0 else { throw RDTrackError.cantCloseLoop }

            let k = -p1.x / v1.x
            let z = p1.z + k * v1.z

            guard k > 0 else { throw RDTrackError.cantCloseLoop }

            return SCNVector3(0, 0, z)
        }

        let px = try solveCrossSection(p1, v1)

        let length0 = length(p0 - px)
        let length1 = length(p1 - px)
        let minLength = min(length0, length1)

        let pp0 = p0 + (length0 - minLength) * v0
        let pp1 = p1 + (length1 - minLength) * v1

        func turnCenterPoint(_ p0: SCNVector3, _ p1: SCNVector3) throws -> SCNVector3 {
            guard p1.x != 0 else { throw RDTrackError.cantCloseLoop }

            let r = p1.x / 2 + (p0.z - p1.z)*(p0.z - p1.z) / p1.x / 2
            return SCNVector3(r, p0.y, p0.z)
        }

        let pc = try turnCenterPoint(pp0, pp1)
        let r = abs(pc.x)

        let a0 = atan2(pp0.z - pc.z, pp0.x - pc.x)
        let a1 = atan2(pp1.z - pc.z, pp1.x - pc.x)
        let da = (a1 - a0).truncatingRemainder(dividingBy: RDFloat(2 * M_PI))

        func clamp(_ a: RDFloat) -> RDFloat {
            if a > CG_PI {
                return a - CG_2PI
            }

            if a < -CG_PI {
                return a + CG_2PI
            }

            return a
        }

        return (dy, length0 - minLength, length1 - minLength, r, clamp(da))
    }

    // TODO Still glitchy and hardly tested
    func closeTheLoop() throws -> [RIDTrackSegment] {
        var segments = [RIDTrackSegment]()

        let p1 = lastPosition

        let (dy, l0, l1, r, a) = try closeLoop(p1, lastDelta / length(lastDelta))
        
        if l1 > 0 {
            segments.append(RDLineSegment(dz: l1, dy: ease(dy)))
        }
        
        segments.append(RDTurnSegment(turns: -a / RDFloat(2 * M_PI), radius: r))
        
        if l0 > 0 {
            segments.append(RDLineSegment(dz: l0))
        }
        
        return segments
    }
}

func testTrackSimple() -> RIDTrack {
    let segmentLength: RDFloat = 10.0
    return RIDTrack(
        segments: [
            RDLineSegment(dz: 10 * segmentLength, width: const(5)),
//            RDTurnSegment(turns: 0.015, radius: segmentLength),
            RDLineSegment(dz: 2 * segmentLength, width: const(5)),
            RDTurnSegment(turns: -0.25, radius: segmentLength),
            RDLineSegment(dz: 3 * segmentLength, width: const(5)),
//            RDLineSegment(dz: segmentLength, dy: ease(5)),
//            RDLineSegment(dz: segmentLength, dy: ease(-5)),
//            RDTurnSegment(turns: -0.75, radius: segmentLength),
//            RDLineSegment(dz: segmentLength),
        ],
        closeTheLoop: false)
}

func testTrack() -> RIDTrack {
    return RIDTrack(
        segments: [
            RDLineSegment(dz: 20, dx: ease(10), width: ease(8, 10)),
            RDTurnSegment(turns: 0.5, radius: 15),
            RDLineSegment(dz: 15, dy: ease(4)),
            RDTurnSegment(turns: 0.05, radius: 28),
            RDLineSegment(dz: 40, dx: ease(5)),
            RDTurnSegment(turns: -0.25, radius: 28, width: ease(10, 20)),
            RDLineSegment(dz: 20, roll: ease(-0.03)),
            RDTurnSegment(turns: -0.5, radius: 10, roll: const(-0.03)),
            RDLineSegment(dz: 20, dy: ease(-4), roll: ease(-0.03, 0), width: ease(20, 8)),
//            RDLineSegment(dz: 80),
//            RDTurnSegment(turns: 0.6, radius: 10),
        ],
        closeTheLoop: true)
}

func testTrack8() -> RIDTrack {
    let segmentLength: RDFloat = 50.0
    return RIDTrack(
        segments: [
            RDLineSegment(dz: segmentLength, width: const(20)),
            RDTurnSegment(turns: 0.75, radius: segmentLength),
            RDLineSegment(dz: segmentLength, dy: ease(5)),
            RDLineSegment(dz: segmentLength, dy: ease(-5)),
            RDTurnSegment(turns: -0.75, radius: segmentLength),
            RDLineSegment(dz: segmentLength),
        ],
        closeTheLoop: false)
}

let ridTracks = [
    "Simple": testTrackSimple,
    "8": testTrack8,
    "Original": testTrack,
]
