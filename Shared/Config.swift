//
//  Config.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/14/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation
import CoreGraphics

let kDebug = false

let RIDSkipMenu = true

let RIDOmniLight = false
let RIDAmbientLight = false
let RIDInitialCamera = RDCamera.Car
let RIDPresentOverlay = false
let RIDTrackResolution: RDFloat = 5
let RIDEnableFloor = true

let RIDCarFrictionSlip: CGFloat = 1.8
let RIDThrottleMax: Float = 1500
let RIDThrottleAlpha: CGFloat = 0.05
let RIDBreakMax: Float = 50
let RIDBreakAlpha: CGFloat = 0.5
let RIDSteeringMax: Float = 0.5
let RIDSteeringAlpha: CGFloat = 0.5 //0.05

let RIDSelectedTrack = testTrack()
