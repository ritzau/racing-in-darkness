//
//  SceneKitUtils.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/14/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

let CG_PI = RDFloat(M_PI)
let CG_2PI = RDFloat(2*M_PI)

func perpendicularXZUnitVector(_ v: SCNVector3) -> SCNVector3 {
    let perp = SCNVector3(v.z, 0, -v.x)
    return perp / length(perp)
}

func + (v: SCNVector3, term: RDFloat) -> SCNVector3 {
    return term + v
}

func + (term: RDFloat, v: SCNVector3) -> SCNVector3 {
    return SCNVector3(term + v.x, term + v.y, term + v.z)
}

func + (a: SCNVector3, b: SCNVector3) -> SCNVector3 {
    return SCNVector3(a.x + b.x, a.y + b.y, a.z + b.z)
}

func - (a: SCNVector3, b:SCNVector3) -> SCNVector3 {
    return SCNVector3(a.x - b.x, a.y - b.y, a.z - b.z)
}

func * (factor: RDFloat, v: SCNVector3) -> SCNVector3 {
    return SCNVector3(factor * v.x, factor * v.y, factor * v.z)
}

func * (v: SCNVector3, factor: RDFloat) -> SCNVector3 {
    return factor * v
}

func / (v: SCNVector3, factor: RDFloat) -> SCNVector3 {
    return SCNVector3(v.x / factor, v.y / factor, v.z / factor)
}

func length(_ v: SCNVector3) -> RDFloat {
    return sqrt(v.z * v.z + v.x * v.x)
}

func * (m: SCNMatrix4, v: SCNVector3) -> SCNVector3 {
    return SCNVector3(
        m.m11 * v.x + m.m21 * v.y + m.m31 * v.z + m.m41,
        m.m12 * v.x + m.m22 * v.y + m.m32 * v.z + m.m42,
        m.m13 * v.x + m.m23 * v.y + m.m33 * v.z + m.m43)
//    return SCNVector3(
//        m.m11 * v.x + m.m12 * v.y + m.m13 * v.z,
//        m.m21 * v.x + m.m22 * v.y + m.m23 * v.z,
//        m.m31 * v.x + m.m32 * v.y + m.m33 * v.z)
}

extension SCNVector3: CustomStringConvertible {
    public var description: String {
        return "[SCNVector3 x=\(x) y=\(y) z=\(z)]"
    }
}
