//
//  BulletUniverse-Details.h
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/26/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#pragma once

#import "BulletUniverse.h"

#include <memory>

#include <btBulletDynamicsCommon.h>

@interface RIDBulletPhysicsWorld (RIDWorldAccess)

@property (readonly) btDynamicsWorld* _Nonnull bulletWorld;

@end

@interface RIDBasicPhysicsBody (RIDBasicInit)

- (id _Nullable) initWithBody: (std::unique_ptr<btRigidBody>) body;

- (id _Nullable) initWithConstructionInfo: (const btRigidBody::btRigidBodyConstructionInfo&) constructionInfo;

- (id _Nullable) initWithShape: (btCollisionShape* _Nonnull) shape mass: (float) mass;

@end
