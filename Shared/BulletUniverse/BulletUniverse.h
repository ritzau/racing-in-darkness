//
//  BulletUniverse.h
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/27/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>

struct btRigidBody;


@protocol RIDBodyDelegate

@property (readonly) struct btRigidBody* _Nonnull body;

@end


@interface RIDPhysicsBody : NSObject

@property (readonly) float x;
@property (readonly) float y;
@property (readonly) float z;
@property (readonly) float qx;
@property (readonly) float qy;
@property (readonly) float qz;
@property (readonly) float qAngle;

- (void) update;

@end


@interface RIDBasicPhysicsBody : RIDPhysicsBody<RIDBodyDelegate>

@end


@interface RIDPhysicsSphere : RIDBasicPhysicsBody

- (id _Nullable) initWithMass: (float) mass radius: (float) radius;

@end


@interface RIDBulletPhysicsPlane : RIDBasicPhysicsBody

- (id _Nullable) initWithNormalX: (float) x y: (float) y z: (float) z distance: (float) distance;

@end


@interface RIDBulletPhysicsWorld : NSObject

- (id _Nonnull) init;

- (void) setup;

- (void) addBody: (RIDPhysicsBody * _Nonnull) body;

- (void) step: (float) time;

@end
