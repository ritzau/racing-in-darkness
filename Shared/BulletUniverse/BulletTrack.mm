//
//  BulletTrack.m
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/31/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#import "BulletTrack.h"

#include <btBvhTriangleMeshShape.h>
#include <btTriangleIndexVertexArray.h>

#import "Log.h"
#import "BulletUniverse-Details.h"

@implementation RIDBulletTrack {
    TRILog* _log;
}

+ (btBvhTriangleMeshShape*) buildShape: (const SCNVector3* _Nonnull) vertices count: (NSInteger) numVertices {
    int numTriangles = (int) numVertices - 2;
    int* indexBase = new int[numVertices];
    int indexStride = sizeof(int);

    btScalar* vertexBase = new btScalar[3 * numVertices];
    int vertexStride = 3 * sizeof(btScalar);

    // TODO Can this step be avoided?
    for (auto i = 0; i < numVertices; ++i) {
        vertexBase[3 * i + 0] = vertices[i].x;
        vertexBase[3 * i + 1] = vertices[i].y;
        vertexBase[3 * i + 2] = vertices[i].z;
    }

    for (auto i = 0; i < numVertices; ++i) {
        indexBase[i] = i;
    }

    auto vertexArray = new btTriangleIndexVertexArray(numTriangles,
                                                      indexBase,
                                                      indexStride,
                                                      (int) numVertices,
                                                      vertexBase,
                                                      vertexStride);

    return new btBvhTriangleMeshShape(vertexArray, false);
}

- (id _Nullable) initWithVertices: (const SCNVector3* _Nonnull) vertices count: (NSInteger) count {
    self = [super initWithShape: [RIDBulletTrack buildShape: vertices count: count] mass: 0.0];

    if (self) {
        _log = [[TRILog alloc] initWithTag: @"BulletTrack"];
//        [_log ifmt: @"count: %ld", (long) count];
//        for (auto i = 0; i < (long) count; ++i) {
//            [_log ifmt: @"v[%d]: x=%f y=%f z=%f", i, vertices[i].x, vertices[i].y, vertices[i].z];
//        }
    }

    return self;
}

@end
