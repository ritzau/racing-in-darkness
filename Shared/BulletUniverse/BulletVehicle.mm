//
//  BulletVehicle.m
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/25/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#import "BulletVehicle.h"
#import "BulletUniverse-Details.h"

#include <vector>

//#import <Cocoa/Cocoa.h>
#import <SceneKit/SceneKit.h>

#include <btMLCPSolver.h>
#include <btList.h>
#include <btRigidBody.h>
#include <btBulletDynamicsCommon.h>
#include <btBoxShape.h>
#include <btStaticPlaneShape.h>

#import "Log.h"

//#import "Racing_In_Darkness_OSX-Swift.h"

namespace {
    TRILog* _log = [[TRILog alloc] initWithTag: @"BulletVehicle"];

    constexpr auto CUBE_HALF_EXTENTS = 1;

    constexpr auto defaultBreakingForce = 10.f;
//    constexpr auto maxEngineForce = 1000.f;
//    constexpr auto maxBreakingForce = 100.f;
//    constexpr auto steeringIncrement = 0.04f;
//    constexpr auto steeringClamp = 0.3f;

    constexpr auto rightIndex = 0;
    constexpr auto upIndex = 1;
    constexpr auto forwardIndex = 2;

    // FIXME
    btVector3 wheelDirectionCS0(0,-1,0);
    btVector3 wheelAxleCS(-1,0,0);
    btScalar suspensionRestLength(0.6);

    using btTuning = btRaycastVehicle::btVehicleTuning;

    btRigidBody* localCreateRigidBody(btScalar mass, const btTransform& startTransform, btCollisionShape* shape) {
        btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

        //rigidbody is dynamic if and only if mass is non zero, otherwise static
        bool isDynamic = (mass != 0.f);

        btVector3 localInertia(0,0,0);
        if (isDynamic)
            shape->calculateLocalInertia(mass,localInertia);

        //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

        btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

        btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);
        cInfo.m_restitution = 0.5;
        
        btRigidBody* body = new btRigidBody(cInfo);
        //body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);

//        m_dynamicsWorld->addRigidBody(body);

        return body;
    }
}

@implementation RIDBulletVehicle {
    @package btRigidBody* m_carChassis;
    btCollisionShape* m_wheelShape;
    btVehicleRaycaster* m_vehicleRayCaster;
    btRaycastVehicle* m_vehicle;
    btTuning m_tuning;

    RIDPhysicsBody* _chassisBody;
}

- (id) initWithWorld: (RIDBulletPhysicsWorld*) world {
    self = [super init];

    if (self) {
        self.gEngineForce = 0.f;
        self.gBreakingForce = 100.f;
        self.gVehicleSteering = 0.f;
        self.wheelRadius = 0.5f;
        self.wheelWidth = 0.2f;
        self.wheelFriction = 1.;//BT_LARGE_FLOAT;
        self.suspensionStiffness = 20.f;
        self.suspensionDamping = 2.3f;
        self.suspensionCompression = 4.4f;
        self.rollInfluence = 0.1f;//1.0f;

        auto m_collisionShapes = std::vector<btCollisionShape*>{};

        //m_dynamicsWorld->setGravity(btVector3(0,0,0));
        btTransform tr;
        tr.setIdentity();
        tr.setOrigin(btVector3(0,-3,0));

        //either use heightfield or triangle mesh


        btCollisionShape* chassisShape = new btBoxShape(btVector3(1.f,0.5f,2.f));
        m_collisionShapes.push_back(chassisShape);

        btCompoundShape* compound = new btCompoundShape();
        m_collisionShapes.push_back(compound);
        btTransform localTrans;
        localTrans.setIdentity();
        //localTrans effectively shifts the center of mass with respect to the chassis
        localTrans.setOrigin(btVector3(0,1,0));

        compound->addChildShape(localTrans,chassisShape);

        {
            btCollisionShape* suppShape = new btBoxShape(btVector3(0.5f,0.1f,0.5f));
            btTransform suppLocalTrans;
            suppLocalTrans.setIdentity();
            //localTrans effectively shifts the center of mass with respect to the chassis
            suppLocalTrans.setOrigin(btVector3(0,1.0,2.5));
            compound->addChildShape(suppLocalTrans, suppShape);
        }

        tr.setOrigin(btVector3(0,0.f,0));

        m_carChassis = localCreateRigidBody(800,tr,compound);//chassisShape);
        //m_carChassis->setDamping(0.2,0.2);
        _chassisBody = [[RIDBasicPhysicsBody alloc] initWithBody: std::unique_ptr<btRigidBody>(m_carChassis)];

        float wheelRadius = self.wheelRadius;
        float wheelWidth = self.wheelWidth;

        m_wheelShape = new btCylinderShapeX(btVector3(wheelWidth,wheelRadius,wheelRadius));

        {
            m_vehicleRayCaster = new btDefaultVehicleRaycaster(world.bulletWorld);
            m_vehicle = new btRaycastVehicle(m_tuning,m_carChassis,m_vehicleRayCaster);

            ///never deactivate the vehicle
            m_carChassis->setActivationState(DISABLE_DEACTIVATION);

            world.bulletWorld->addVehicle(m_vehicle);

            float connectionHeight = 1.2f;


            bool isFrontWheel=true;

            //choose coordinate system
            m_vehicle->setCoordinateSystem(rightIndex,upIndex,forwardIndex);

            btVector3 connectionPointCS0(CUBE_HALF_EXTENTS-(0.3*wheelWidth),connectionHeight,2*CUBE_HALF_EXTENTS-wheelRadius);

            m_vehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,m_tuning,isFrontWheel);
            connectionPointCS0 = btVector3(-CUBE_HALF_EXTENTS+(0.3*wheelWidth),connectionHeight,2*CUBE_HALF_EXTENTS-wheelRadius);

            m_vehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,m_tuning,isFrontWheel);
            connectionPointCS0 = btVector3(-CUBE_HALF_EXTENTS+(0.3*wheelWidth),connectionHeight,-2*CUBE_HALF_EXTENTS+wheelRadius);
            
            isFrontWheel = false;
            m_vehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,m_tuning,isFrontWheel);
            connectionPointCS0 = btVector3(CUBE_HALF_EXTENTS-(0.3*wheelWidth),connectionHeight,-2*CUBE_HALF_EXTENTS+wheelRadius);
            m_vehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,m_tuning,isFrontWheel);
            
            for (int i=0;i<m_vehicle->getNumWheels();i++)
            {
                btWheelInfo& wheel = m_vehicle->getWheelInfo(i);
                wheel.m_suspensionStiffness = self.suspensionStiffness;
                wheel.m_wheelsDampingRelaxation = self.suspensionDamping;
                wheel.m_wheelsDampingCompression = self.suspensionCompression;
                wheel.m_frictionSlip = self.wheelFriction;
                wheel.m_rollInfluence = self.rollInfluence;
            }
        }
    }

    return self;
}

- (RIDBasicPhysicsBody*) chassisBody {
    return (RIDBasicPhysicsBody*) _chassisBody;
}

- (void) resetTo: (SCNVector3) pos world: (RIDBulletPhysicsWorld*) world {
    self.gVehicleSteering = 0.f;
    self.gBreakingForce = defaultBreakingForce;
    self.gEngineForce = 0.f;

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(pos.x, pos.y, pos.z));
    m_carChassis->setCenterOfMassTransform(transform);
    m_carChassis->setLinearVelocity(btVector3(0,0,0));
    m_carChassis->setAngularVelocity(btVector3(0,0,0));
    world.bulletWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(m_carChassis->getBroadphaseHandle(),world.bulletWorld->getDispatcher());

    if (m_vehicle)
    {
        m_vehicle->resetSuspension();
        for (int i=0;i<m_vehicle->getNumWheels();i++)
        {
            //synchronize the wheels with the (interpolated) chassis worldtransform
            m_vehicle->updateWheelTransform(i,true);
        }
    }
}

- (void) stepSimulation: (RIDBulletPhysicsWorld*) world deltaTime: (float) deltaTime {
//    [_log vfmt:@"step %f %f %f", self.gEngineForce, self.gBreakingForce, self.gVehicleSteering];

    {
        int wheelIndex = 2;
        m_vehicle->applyEngineForce(self.gEngineForce,wheelIndex);
        m_vehicle->setBrake(self.gBreakingForce,wheelIndex);
        wheelIndex = 3;
        m_vehicle->applyEngineForce(self.gEngineForce,wheelIndex);
        m_vehicle->setBrake(self.gBreakingForce,wheelIndex);


        wheelIndex = 0;
        m_vehicle->setSteeringValue(self.gVehicleSteering,wheelIndex);
        wheelIndex = 1;
        m_vehicle->setSteeringValue(self.gVehicleSteering,wheelIndex);

    }

    float dt = deltaTime;

    if (world.bulletWorld)
    {
        //during idle mode, just run 1 simulation step maximum
        int maxSimSubSteps = 2;

        int numSimSteps;
        numSimSteps = world.bulletWorld->stepSimulation(dt,maxSimSubSteps);

        if (world.bulletWorld->getConstraintSolver()->getSolverType()==BT_MLCP_SOLVER)
        {
            btMLCPSolver* sol = (btMLCPSolver*) world.bulletWorld->getConstraintSolver();
            int numFallbacks = sol->getNumFallbacks();
            if (numFallbacks)
            {
                static int totalFailures = 0;
                totalFailures+=numFallbacks;
                printf("MLCP solver failed %d times, falling back to btSequentialImpulseSolver (SI)\n",totalFailures);
            }
            sol->setNumFallbacks(0);
        }


//#define VERBOSE_FEEDBACK
#ifdef VERBOSE_FEEDBACK
        if (!numSimSteps)
            printf("Interpolated transforms\n");
        else
        {
            if (numSimSteps > maxSimSubSteps)
            {
                //detect dropping frames
                printf("Dropped (%i) simulation steps out of %i\n",numSimSteps - maxSimSubSteps,numSimSteps);
            } else
            {
                [_log vfmt: @"Simulated (%i) steps\n", numSimSteps];
            }
        }
#endif //VERBOSE_FEEDBACK
        
    }
}

@end
