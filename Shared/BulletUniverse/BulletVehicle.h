//
//  BulletVehicle.h
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/25/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

#import "BulletUniverse.h"

@interface RIDBulletVehicle : NSObject

@property float gEngineForce;
@property float gBreakingForce;
@property float gVehicleSteering;

@property float wheelRadius;
@property float wheelWidth;
@property float wheelFriction;
@property float suspensionStiffness;
@property float suspensionDamping;
@property float suspensionCompression;
@property float rollInfluence;

@property (readonly) RIDPhysicsBody* chassisBody;

- (id) initWithWorld: (RIDBulletPhysicsWorld*) world;

- (void) resetTo: (SCNVector3) pos world: (RIDBulletPhysicsWorld*) world;

- (void) stepSimulation: (RIDBulletPhysicsWorld*) world deltaTime: (float) deltaTime;

@end