//
//  BulletUniverse.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/25/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation

import SceneKit;

private let _log = TRILog(tag: "Universe")

extension RIDPhysicsBody: RIDPhysicsObject {
}

extension RIDBulletVehicle {
    func willStepSimulation(_ controls: RIDGameInputController) {
        gEngineForce = RIDThrottleMax * controls.throttle
        gBreakingForce = RIDBreakMax * controls.breaking
        gVehicleSteering = RIDSteeringMax * controls.steering
    }
}

class RIDBulletUniverse: RIDPhysicsUniverse {
    let _physics = RIDBulletPhysicsWorld()
    let _playerControls: RIDGameInputController

    // FIXME
    let _vehicle: RIDBulletVehicle


    init(playerControls: RIDGameInputController) {
        _vehicle = RIDBulletVehicle(world: _physics)
        _playerControls = playerControls
    }

    func step(_ deltaTimeSeconds: Float) {
        _vehicle.willStepSimulation(_playerControls)
        _vehicle.stepSimulation(_physics, deltaTime: deltaTimeSeconds)
        _physics.step(deltaTimeSeconds)
    }
    
    func createBall(_ radius: Float) -> RIDPhysicsObject {
        return createBody(RIDPhysicsSphere(mass: 1.0, radius: radius)!)
    }
    
    func createFloor(_ y: Float) -> RIDPhysicsObject {
        return createBody(RIDBulletPhysicsPlane(normalX: 0, y: 1, z: 0, distance: y)!)
    }

    func createVehicleAt(_ pos: SCNVector3) -> RIDPhysicsObject {
        _vehicle.reset(to: pos, world: _physics)
        return createBody(_vehicle.chassisBody)
    }

    func createTrackSegmentAt(_ origin: SCNVector3, rotation: SCNVector4, vertices: [SCNVector3]) -> RIDPhysicsObject {
//        _log.i("in swift count=\(vertices.count)")

        return createBody(RIDBulletTrack(vertices: vertices, count: vertices.count)!)
    }

    fileprivate func createBody(_ body: RIDPhysicsBody) -> RIDPhysicsObject {
        _physics.add(body)
        return body;
    }
}
