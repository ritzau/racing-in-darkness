//
//  BulletUniverse.m
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/27/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#import "BulletUniverse-Details.h"

#include <iomanip>
#include <iostream>
#include <memory>

#include <btList.h>
#include <btRigidBody.h>
#include <btBulletDynamicsCommon.h>
#include <btBoxShape.h>
#include <btStaticPlaneShape.h>

#import "Log.h"

using namespace std;

@interface RIDPhysicsBody ()

@property (readonly) btRigidBody* body;

- (id) initWithDelegate: (id <RIDBodyDelegate>) delegate;

@end


@implementation RIDPhysicsBody {
    btTransform _transform;
    __weak id <RIDBodyDelegate> _delegate; // FIXME weak?
    TRILog* _log;
}

- (id) initWithDelegate: (id <RIDBodyDelegate>) delegate {
    self = [super init];

    if (self) {
        NSAssert(delegate, @"Delegate must be non-null");
        _delegate = delegate;
    }

    return self;
}

- (btRigidBody*) body {
    NSAssert(_delegate, @"Delegate-less body");
    return _delegate.body;
}

- (void) update {
    btRigidBody* body = self.body;

    NSAssert(body, @"Bodyless body");
    auto state = body->getMotionState();

    NSAssert(state, @"Body without motion state");
    state->getWorldTransform(_transform);
    [_log vfmt:@"trans %f", _transform.getOrigin().getZ()];
}

- (float) x {
    return _transform.getOrigin().getX();
}

- (float) y {
    return _transform.getOrigin().getY();
}

- (float) z {
    return _transform.getOrigin().getZ();
}

- (float) qx {
    return _transform.getRotation().getAxis().getX();
}

- (float) qy {
    return _transform.getRotation().getAxis().getY();
}

- (float) qz {
    return _transform.getRotation().getAxis().getZ();
}

- (float) qAngle {
    return _transform.getRotation().getAngle();
}


@end


//@interface RIDBasicPhysicsBody ()
//
////@property (readonly) btRigidBody* body;
//
//@end


@implementation RIDBasicPhysicsBody {
    std::unique_ptr<btRigidBody> _body;
}

- (id _Nullable) initWithBody: (std::unique_ptr<btRigidBody>) body {
    self = [super initWithDelegate: self];

    if (self) {
        _body = std::move(body);
    }

    return self;
}

- (id _Nullable) initWithConstructionInfo: (const btRigidBody::btRigidBodyConstructionInfo&) constructionInfo {
    return [self initWithBody: std::make_unique<btRigidBody>(constructionInfo)];
}

- (id _Nullable) initWithShape: (btCollisionShape*) shape mass: (float) mass {
    auto info = btRigidBody::btRigidBodyConstructionInfo{
        mass,
        new btDefaultMotionState{}, // XXX Where does this go?
        shape};
    info.m_restitution = 0.7;
    return [self initWithConstructionInfo: info];
}

- (btRigidBody*) body {
    return _body.get();
}

@end



@implementation RIDPhysicsSphere

- (id _Nullable) initWithMass: (float) mass radius: (float) radius {
    self = [super initWithShape: new btSphereShape{radius} mass: mass];
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(0, 40, 15));
    self.body ->setWorldTransform(transform);

    return self;
}

@end


@implementation RIDBulletPhysicsPlane {
    float _distance;
}

- (id _Nullable) initWithNormalX: (float) x y: (float) y z: (float) z distance: (float) distance {
    _distance = distance;

    return [super initWithShape: new btStaticPlaneShape{btVector3{x, y, z}, distance} mass: 0];
}

- (float) y {
    return _distance;
}

@end


@implementation RIDBulletPhysicsWorld {
    unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration;
    unique_ptr<btCollisionDispatcher> dispatcher;
    unique_ptr<btDbvtBroadphase> overlappingPairCache;
    unique_ptr<btSequentialImpulseConstraintSolver> solver;
    unique_ptr<btDiscreteDynamicsWorld> dynamicsWorld;
}

- (id)init {
    self = [super init];

    if (self) {
        collisionConfiguration = make_unique<btDefaultCollisionConfiguration>();

        ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
        dispatcher = make_unique<btCollisionDispatcher>(collisionConfiguration.get());
        ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
        overlappingPairCache = make_unique<btDbvtBroadphase>();
        ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
        solver = make_unique<btSequentialImpulseConstraintSolver>();

        dynamicsWorld = make_unique<btDiscreteDynamicsWorld>(dispatcher.get(),
                                                             overlappingPairCache.get(),
                                                             solver.get(),
                                                             collisionConfiguration.get());
        dynamicsWorld->setGravity(btVector3{0, -10, 0});
    }

    return self;
}

-(void)setup {
    dynamicsWorld->setGravity(btVector3{0, -10, 0});
}


- (void) addBody: (RIDPhysicsBody * _Nonnull) body {
    dynamicsWorld->addRigidBody(body.body);
}

- (void) step: (float) time {
    dynamicsWorld->stepSimulation(time);
}

@end

@implementation RIDBulletPhysicsWorld (RIDWorldAccess)

- (btDynamicsWorld*) bulletWorld {
    return dynamicsWorld.get();
}

@end