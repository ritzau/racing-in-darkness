//
//  BulletTrack.h
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/31/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

#pragma once

#include <SceneKit/SceneKit.h>

#import "BulletUniverse.h"

@interface RIDBulletTrack : RIDBasicPhysicsBody

- (id _Nullable) initWithVertices: (const SCNVector3* _Nonnull) vertices count: (NSInteger) count;

@end