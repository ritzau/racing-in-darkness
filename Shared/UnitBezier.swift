//
//  UnitBezier.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/9/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//
//  Converted with pride from: https://github.com/WebKit/webkit/blob/master/Source/WebCore/platform/graphics/UnitBezier.h
//

import CoreGraphics

struct UnitBezier {
    
    fileprivate let ax: RDFloat
    fileprivate let bx: RDFloat
    fileprivate let cx: RDFloat
    
    fileprivate let ay: RDFloat
    fileprivate let by: RDFloat
    fileprivate let cy: RDFloat

    init(x1: RDFloat, y1: RDFloat, x2: RDFloat, y2: RDFloat) {
        // Calculate the polynomial coefficients, implicit first and last control points are (0,0) and (1,1).
        cx = 3.0 * x1
        bx = 3.0 * (x2 - x1) - cx
        ax = 1.0 - cx - bx
        
        cy = 3.0 * y1
        by = 3.0 * (y2 - y1) - cy
        ay = 1.0 - cy - by
    }
    
    static func ease() -> UnitBezier {
        return UnitBezier(x1: 0.5, y1: 0, x2: 0.5, y2: 1.0)
    }
    
    func sampleCurveX(_ t: RDFloat) -> RDFloat {
        // `ax t^3 + bx t^2 + cx t' expanded using Horner's rule.
        return ((ax * t + bx) * t + cx) * t
    }
    
    func sampleCurveY(_ t: RDFloat) -> RDFloat {
        return ((ay * t + by) * t + cy) * t
    }
    
    func sampleCurveDerivativeX(_ t: RDFloat) -> RDFloat {
        return (3.0 * ax * t + 2.0 * bx) * t + cx
    }
    
    // Given an x value, find a parametric value it came from.
    func solveCurveX(_ x: RDFloat, epsilon: RDFloat) -> RDFloat {
        var t0: RDFloat
        var t1: RDFloat
        var t2: RDFloat
        var x2: RDFloat
        var d2: RDFloat
        
        // First try a few iterations of Newton's method -- normally very fast.
        t2 = x
        for _ in 0..<8 {
            x2 = sampleCurveX(t2) - x
            if abs(x2) < epsilon {
                return t2
            }
            d2 = sampleCurveDerivativeX(t2)
            if abs(d2) < 1e-6 {
                break;
            }
            t2 = t2 - x2 / d2
        }
        
        // Fall back to the bisection method for reliability.
        t0 = 0.0
        t1 = 1.0
        t2 = x
        
        if t2 < t0 {
            return t0
        }
        if t2 > t1 {
            return t1
        }
        
        while t0 < t1 {
            x2 = sampleCurveX(t2)
            if abs(x2 - x) < epsilon {
                return t2
            }
            if x > x2 {
                t0 = t2
            }
            else {
                t1 = t2;
            }
            t2 = (t1 - t0) * 0.5 + t0
        }
        
        // Failure.
        return t2
    }
    
    func solve(_ x: RDFloat, epsilon: RDFloat) -> RDFloat {
        return sampleCurveY(solveCurveX(x, epsilon: epsilon));
    }
}
