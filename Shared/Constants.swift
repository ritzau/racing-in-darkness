//
//  Constants.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/11/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation

enum RDCamera: String {
    case Static = "Static Camera"
    case Ortho = "Ortho Camera"
    case Car = "Car Camera"
}

let CATEGORY_TRACK: Int = 1 << 0
let CATEGORY_CAR: Int = 1 << 1
let CATEGORY_MARKER: Int = 1 << 2

