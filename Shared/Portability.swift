//
//  Portability.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/18/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation

#if os(OSX)
    import AppKit
    
    typealias RDColor = NSColor
    typealias RDImage = NSImage
    typealias RDFloat = CGFloat
    #else
    import UIKit
    
    typealias RDColor = UIColor
    typealias RDImage = UIImage
    typealias RDFloat = Float
#endif
