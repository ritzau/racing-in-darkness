//
//  SceneKitUniverse.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/25/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

private let _log = TRILog.get()

func createCamera(_ camera: RDCamera, position: SCNVector3) -> SCNNode {
    _log.v("pos=\(position)")
    _log.v("foo \(1) \(2) \(3)")

    let node = SCNNode()
    node.position = position
    node.name = camera.rawValue
    node.camera = SCNCamera()
    node.camera!.zFar = 1000
    node.camera!.categoryBitMask = ~CATEGORY_MARKER

    return node
}

func createOrthoCamera(_ camera: RDCamera, position: SCNVector3) -> SCNNode {
    let node = createCamera(camera, position: position)
    node.position = SCNVector3(x: -40, y: 50, z: 5)
    node.eulerAngles.x = RDFloat(-M_PI / 2)
    node.camera!.usesOrthographicProjection = true
    node.camera!.orthographicScale = 160

    return node
}

func createAmbientLight(_ color: RDColor) -> SCNNode {
    let light = SCNLight()
    light.type = SCNLight.LightType.ambient
    light.color = color

    let node = SCNNode()
    node.light = light

    return node
}

func createOmniLight(_ position: SCNVector3) -> SCNNode {
    let light = SCNLight()
    light.type = SCNLight.LightType.omni

    let node = SCNNode()
    node.position = position
    node.light = light

    return node
}

extension SCNNode: RIDGraphicsObject {
    func update(_ body: RIDPhysicsObject) {
        position = SCNVector3(body.x, body.y, body.z)
        rotation = SCNVector4(body.qx, body.qy, body.qz, body.qAngle)
//        _log.v("[Node x=\(body.x), y=\(body.y), z=\(body.z)]")
    }
}

class RIDSceneKitUniverse: RIDGraphicsUniverse {
    let scene: SCNScene

    init(scene: SCNScene) {
        //        root.addChildNode(createCamera(.Static, position: SCNVector3(x: 2, y: 2, z: 15)))
        self.scene = scene

        let root = scene.rootNode

        root.addChildNode(createOrthoCamera(.Ortho, position: SCNVector3(x: -40, y: 50, z: 5)))
        if RIDAmbientLight {
            root.addChildNode(createAmbientLight(RDColor.darkGray))
        }
        if RIDOmniLight {
            root.addChildNode(createOmniLight(SCNVector3(x: 0, y: 10, z: 10)))
        }
    }

    func registerPhysicsCallback(_ callback: @escaping () -> Void) {
        let action = SCNAction.customAction(duration: TimeInterval.infinity) {
            (_, _) -> Void in callback()
        }
        scene.rootNode.runAction(action)
    }

    func createBall(_ radius: Float) -> RIDGraphicsObject {
        let sphere = SCNSphere(radius: CGFloat(radius))
//        sphere.
        return createNode(geometry: sphere)
    }
    
    func createFloor() -> RIDGraphicsObject {
        let floor = SCNFloor()
        floor.reflectivity = 0
        return createNode(geometry: floor, category: CATEGORY_TRACK)
    }

    func createVehicle() -> RIDGraphicsObject {
        let cameraNode = createCamera(.Car, position: SCNVector3(x: 0, y: 2, z: 0))
        cameraNode.eulerAngles.y = RDFloat(M_PI)

        let node = createNode(geometry: SCNBox(width: 1.8, height: 1.2, length: 4, chamferRadius: 0.1))
        node.addChildNode(cameraNode)

        return node
    }

    func createTrackSegment(_ segmentId: Int, vertices: [(left: SCNVector3, center: SCNVector3, right: SCNVector3)]) -> RIDGraphicsObject {
        return createNode(renderSegment(segmentId, vertices: vertices))
    }

    fileprivate func createNode(geometry: SCNGeometry, category: Int = -1) -> SCNNode {
        let node = SCNNode()
        node.geometry = geometry
        node.categoryBitMask = category
        return createNode(node)
    }

    fileprivate func createNode(_ node: SCNNode) -> SCNNode {
        scene.rootNode.addChildNode(node)
        return node
    }
}

