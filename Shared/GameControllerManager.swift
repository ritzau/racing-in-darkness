//
//  GameControllerManager.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/31/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation
import GameController

enum ControllerError: Error {
    case incompatibleController
}

protocol RIDGameControllerDelegate {
    func didFindController()
    func onControllerDisconnected()
}

class RIDGameControllerManager {
    let _log = TRILog(tag: "gcmgr")

    var delegate: RIDGameControllerDelegate?

    fileprivate var _connectionObserver: NSObjectProtocol?
    fileprivate var _disconnectionObserver: NSObjectProtocol?

    func startControllerSearch() {
        _log?.d("Searching for controllers")
        
        _connectionObserver = NotificationCenter.default.addObserver(
            forName: NSNotification.Name.GCControllerDidConnect,
            object: nil,
            queue: OperationQueue.main,
            using: {_ in
                self._log?.d("Found controller")
                self.delegate?.didFindController()
        })
    }

    func stopControllerSearch() {
        _log?.d("Stop searching for controllers")

        GCController.stopWirelessControllerDiscovery()

        let notificationCenter = NotificationCenter.default

        if let connectionObserver = _connectionObserver {
            notificationCenter.removeObserver(connectionObserver)
        }

        if let disconnectionObserver = _disconnectionObserver {
            notificationCenter.removeObserver(disconnectionObserver)
        }
    }
}
