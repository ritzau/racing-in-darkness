//
//  GameViewConfiguration.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 11/28/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import GameController
import SceneKit
import SpriteKit

class RIDRacingInDarknessImpl: RIDRacingInDarkness, RIDMenuInputController, RIDMenuDelegate, RIDGameControllerDelegate {

    fileprivate let _log = TRILog(tag: "rid")

    let overlay: RIDOverlay
    var user: RIDUser? = nil
    var game: RIDGame? = nil

    fileprivate let _controllerManager = RIDGameControllerManager()
    fileprivate let _scene: SCNScene
    fileprivate let _view: SCNView

    init(scene: SCNScene, gameView: SCNView, overlay: RIDOverlay) {
        self._scene = scene
        self._view = gameView
        self.overlay = overlay

        _controllerManager.delegate = self
        _controllerManager.startControllerSearch() // XXX separate function?

        if let menu = overlay.menu {
            menu.menuDelegate = self
        }

        let action = SCNAction.customAction(duration: TimeInterval.infinity) {
            (_, _) -> Void in
        }

        scene.rootNode.runAction(action)

        if RIDSkipMenu {
            overlay.hideMenu()
            startGame()
        }
    }

    var inputMode: RIDInputMode {
        return overlay.isPresentingMenu ? .menu : .game
    }

    var menuInputController: RIDMenuInputController {
        return self
    }

    var gameInputController: RIDGameInputController? {
        return game?.controls
    }


    func startGame() {
        _log?.i("Start game")
        game = RIDGameImpl(scene: _scene, overlay: overlay)
        _view.pointOfView = game!.carCamera // FIXME From config
        game!.start()
    }

    func didCancelMenu() {
        // TODO
        overlay.hideMenu()
    }

    func didActivateAction(_ action: String) {
        overlay.hideMenu()
        startGame()
    }

    func pressKey(_ key: RIDMenuInputKey) {
        guard let menu = overlay.menu else {
            preconditionFailure("No menu")
        }

        precondition(inputMode == .menu)

        switch (key) {
        case .up:
            menu.selectPrevious()
            break
        case .down:
            menu.selectNext()
            break
        case .back:
            menu.back()
            break
        case .enter:
            menu.activateSelectedItem()
            break
        }
    }

    func activateMenuItemAtIndex(_ index: Int) {
        overlay.menu?.activateItemAtIndex(index)
    }

    func didFindController() {
        func handleDpad(_ key: RIDMenuInputKey, _ pressed: Bool) {
            if inputMode == .menu && pressed {
                self.menuInputController.pressKey(key)
            }
        }
        
        guard let controller = GCController.controllers().first,
                  let extendedGamepad = controller.extendedGamepad else
        {
            return
        }

        controller.controllerPausedHandler = { _ in
            trilog.e("Pause")
            // self._player.controls.resetCarToStart()
        }

        _controllerManager.stopControllerSearch()

        extendedGamepad.valueChangedHandler = { pad, element in
            self.gameControllerValueChanged(pad, element)
        }

        let dpad = extendedGamepad.dpad
        let buttons: [(btn: GCControllerButtonInput, key: RIDMenuInputKey)] = [
            (dpad.up, .up), (dpad.down, .down), (dpad.left, .back), (dpad.right, .enter)
        ]
        buttons.forEach { bk in
            bk.btn.pressedChangedHandler = { _, _, pressed in handleDpad(bk.key, pressed) }
        }
    }

    func onControllerDisconnected() {
        // TODO
    }

    func gameControllerValueChanged(_ pad: GCExtendedGamepad, _ element: GCControllerElement) {
        func handleMenuInput() {
            switch element {
            case pad.buttonA:
                menuInputController.pressKey(.enter)
                break

            default:
                // Ignore
                break
            }
        }

        func handleGameInput() {
            switch element {
            case pad.buttonA:
                gameInputController?.throttle = Float((element as! GCControllerButtonInput).value)
                break

            case pad.buttonB:
                gameInputController?.breaking = Float((element as! GCControllerButtonInput).value)
                break

            case pad.leftThumbstick:
                gameInputController?.steering = -Float((element as! GCControllerDirectionPad).xAxis.value)
                break

            default:
                // Ignore
                break
            }
        }

        _log?.v("controller input mode=\(inputMode) element=\(element)")

        switch inputMode {
        case .menu:
            handleMenuInput()
            break
        case .game:
            handleGameInput()
            break
        case .none:
            break
        }
    }
}

extension SCNView {
    func setupSceneView(_ gameScene: SCNScene, overlayScene: SKScene) {
        allowsCameraControl = false
        showsStatistics = true
        backgroundColor = RDColor.black
        debugOptions.insert(SCNDebugOptions.showLightExtents)
        scene = gameScene
        overlaySKScene = overlayScene
        pointOfView = gameScene.rootNode.childNode(withName: RIDInitialCamera.rawValue, recursively: true)
    }
}

extension GameView {
    func setup(inputHandler: RIDInputHandler, overlay: RIDOverlayMenuView & RIDButtonOverlay) {
        _inputHandler = inputHandler
        _overlay = overlay
    }
}

func launchRacingInDarknessInGameView(_ gameView: GameView, hasTouch: Bool) {
    let scene = SCNScene()
    let overlayScene = SKScene(size: gameView.frame.size)

    let overlay = RIDOverlayImpl(overlayScene: overlayScene, hasTouch: hasTouch)
    let racingInDarkness = RIDRacingInDarknessImpl(scene: scene, gameView: gameView, overlay: overlay)

    gameView.setupSceneView(scene, overlayScene: overlayScene)
    gameView.setup(inputHandler: racingInDarkness, overlay: overlay)
}
