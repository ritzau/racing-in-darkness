//
//  Player.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/11/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import GameController

class RIDPlayerImpl: RIDPlayer, CustomStringConvertible {
    let controls: RIDGameInputController

    let user: RIDUser? = nil

    var startTime: Date?
    var lapStartTime: Date?
    var lastLapTime: TimeInterval = 0
    var lastMarkHit: String?
    
    init(controls: RIDGameInputController) {
        self.controls = controls
    }

    func startRace() {
        startTime = Date()
        lapStartTime = Date()
    }
 
    func hitMark(_ mark: String) {
        if lastMarkHit != "Mark 0" && mark == "Mark 0" {
            lastLapTime = -(lapStartTime?.timeIntervalSinceNow ?? 0)
            lapStartTime = Date()
        }
        lastMarkHit = mark
    }
    
    var elapsedRacingTime: TimeInterval {
        return -(startTime?.timeIntervalSinceNow ?? 0)
    }

    var velocity: Float { return 0 }

    var description: String { return "banana" }
}
