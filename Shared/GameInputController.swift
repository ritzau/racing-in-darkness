//
//  GameInputController.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 12/6/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation

class RIDGameInputControllerImpl: RIDGameInputController {
    var throttle: Float = 0
    var breaking: Float = 0
    var steering: Float = 0

    func pressKey(_ key: RIDGameInputKey) {
        switch key {
        case .throttle:
            throttle = 1.0
            breaking = 0.0
            break
        case .break:
            throttle = 0.0
            breaking = 1.0
        case .steerLeft:
            steering = 1.0
            break
        case .steerRight:
            steering = -1.0
            break
        default:
            break
        }
    }

    func releaseKey(_ key: RIDGameInputKey) {
        switch key {
        case .throttle:
            throttle = 0.0
            break
        case .break:
            breaking = 0.0
        case .steerLeft:
            steering = 0.0
            break
        case .steerRight:
            steering = 0.0
            break
        default:
            break
        }
    }
}
