//
//  RIDOverlayMenu.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 11/29/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation
import SpriteKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


protocol RIDOverlayMenuView {
    func menuItemIndexAtPoint(_ point: CGPoint) -> Int?
}

protocol RIDMenuViewDelegate: class {
    func didActivateMenu(_ menu: RIDSubmenuItem)
    func didActivateAction(_ item: String)
    func didSelectIndex(_ oldIndex: Int?, newIndex: Int?)
}

protocol RIDMenuDelegate: class {
    func didCancelMenu()
    func didActivateAction(_ action: String) // FIXME
}

struct RIDOverlayMenuLabel {
    let text: String
    // TODO format

    init(_ text: String) {
        self.text = text
    }
}

protocol RIDOverlayMenuItem: CustomStringConvertible {
    var label: RIDOverlayMenuLabel {get}
    func activate(_ menu: RIDMenuViewDelegate?)
}

extension RIDOverlayMenuItem {
    var description: String {
        return label.text
    }
}

struct RIDOverlayMenuActionItem: RIDOverlayMenuItem {
    let label: RIDOverlayMenuLabel

    init(_ label: RIDOverlayMenuLabel) {
        self.label = label
    }

    func activate(_ delegate: RIDMenuViewDelegate?) {
        delegate?.didActivateAction(label.text) // FIXME
    }
}

struct RIDSubmenuItem: RIDOverlayMenuItem {
    let label: RIDOverlayMenuLabel
    let children: [RIDOverlayMenuItem]

    init(_ label: RIDOverlayMenuLabel, children: [RIDOverlayMenuItem] = []) {
        self.label = label
        self.children = children
    }

    func activate(_ delegate: RIDMenuViewDelegate?) {
        delegate?.didActivateMenu(self)
    }
}

class RIDMenuImpl: RIDMenu, RIDMenuViewDelegate {
    let _log = TRILog(tag: "menu")

    fileprivate var _parents: [(RIDSubmenuItem, Int?)] = []

    var currentMenu: RIDSubmenuItem {
        didSet {
            selectedIndex = nil
        }
    }

    weak var viewDelegate: RIDMenuViewDelegate? = nil
    weak var menuDelegate: RIDMenuDelegate? = nil

    var selectedIndex: Int? = nil {
        didSet {
            // FIXME comparing optionals
            guard selectedIndex == nil || 0 <= selectedIndex && selectedIndex < currentMenu.children.count else {
                selectedIndex = oldValue
                return
            }

            viewDelegate?.didSelectIndex(oldValue, newIndex: selectedIndex)
        }
    }

    var selectedItem: RIDOverlayMenuItem? {
        get {
            guard let index = selectedIndex else {
                return nil
            }

            return currentMenu.children[index]
        }
    }

    init(root: RIDSubmenuItem) {
        self.currentMenu = root
    }

    func selectNext() {
        if let currentIndex = selectedIndex {
            selectedIndex = currentIndex + 1
        } else {
            selectedIndex = 0
        }
    }

    func selectPrevious() {
        if let currentIndex = selectedIndex {
            selectedIndex = currentIndex - 1
        } else {
            selectedIndex = currentMenu.children.count - 1
        }
    }

    func back() {
        if let (parentMenu, index) = _parents.popLast() {
            _log?.d("up \(_parents)")
            currentMenu = parentMenu
            currentMenu.activate(viewDelegate)
            selectedIndex = index
        }
    }

    func activateSelectedItem() {
        selectedItem?.activate(self)
    }

    func activateItemAtIndex(_ index: Int) {
        _log?.d("Activate index: \(index)")
        return currentMenu.children[index].activate(self)
    }
    

    func didActivateAction(_ item: String) {
        viewDelegate?.didActivateAction(item)
        menuDelegate?.didActivateAction(item)
    }

    func didActivateMenu(_ menu: RIDSubmenuItem) {
        _parents.append((currentMenu, selectedIndex))
        _log?.d("down \(_parents)")
        currentMenu = menu
        viewDelegate?.didActivateMenu(menu)
    }

    func didSelectIndex(_ oldIndex: Int?, newIndex: Int?) {
        preconditionFailure()
    }
}

class RIDSpriteKitOverlayMenu: RIDOverlayMenuView, RIDMenuViewDelegate {
    let _log = TRILog(tag: "menu")

    let parentNode: SKNode

    init(parentNode: SKNode) {
        self.parentNode = parentNode
    }

    func menuItemIndexAtPoint(_ point: CGPoint) -> Int? {
        var root = parentNode
        while root.parent != nil { root = root.parent! }

//        let to = parentNode.convertPoint(point, toNode: nil)
        let node = root.atPoint(point)
        _log?.v("point=\(point)")
        _log?.v("node=\(node)")
        _log?.v("children=\(parentNode.children)")
        let index = parentNode.children.index {
            _log?.v("Node: \($0) \($0 === node) \($0 == node)")
            return $0 === node
        }
        _log?.v("index=\(index)")
        return index
    }

    func didActivateMenu(_ submenu: RIDSubmenuItem) {
        buildMenu(submenu)
    }

    func didActivateAction(_ item: String) {
        _log?.d("Activated item: \(item)")
    }

    func didSelectIndex(_ oldIndex: Int?, newIndex: Int?) {
        if let index = oldIndex {
            if let node = parentNode.children[index] as? SKLabelNode {
                node.fontColor = RDColor.green
            }
        }

        if let index = newIndex {
            if let node = parentNode.children[index] as? SKLabelNode {
                node.fontColor = RDColor.red
            }
        }
    }

    func buildMenu(_ submenuItem: RIDSubmenuItem) {
        let x: CGFloat = 100
        var y: CGFloat = 100

        parentNode.removeAllChildren()
        for item in submenuItem.children {
            let node = SKLabelNode(fontNamed: "ChalkDuster")
            node.position = CGPoint(x: x, y: y)
            node.text = item.label.text
            node.fontColor = RDColor.green
            parentNode.addChild(node)

            y -= 1.5 * node.frame.height
        }
    }

}

func buildSampleMenu(_ parentNode: SKNode) -> RIDMenu {
    typealias Menu = RIDSubmenuItem
    typealias Action = RIDOverlayMenuActionItem
    typealias Label = RIDOverlayMenuLabel

    return RIDMenuImpl(
        root: Menu(Label("root"),
            children: [
                Menu(Label("Racing in Darkness"),
                    children: [
                        Menu(Label("Play"),
                            children: [
                                Action(Label("Test track")),
                                Action(Label("Test track 8")),
                            ]),
                        Menu(Label("Settings"),
                            children: [
                                Action(Label("Connect controller")),
                            ]),
                    ]),
            ]))
}
