//
//  Universe.swift
//  Racing In Darkness
//
//  Created by Tobias Ritzau on 10/25/15.
//  Copyright © 2015 Tobias Ritzau. All rights reserved.
//

import Foundation

private let _log = TRILog.get()

@objc protocol RIDPhysicsObject {
    var x: Float { get }
    var y: Float { get }
    var z: Float { get }
    var qx: Float { get }
    var qy: Float { get }
    var qz: Float { get }
    var qAngle: Float { get }

    func update()
}

protocol RIDGraphicsObject {
    func update(_ body: RIDPhysicsObject)
}

protocol RIDPhysicsUniverse {
    func step(_ deltaTimeSeconds: Float)
    
    func createBall(_ radius: Float) -> RIDPhysicsObject
    func createFloor(_ y: Float) -> RIDPhysicsObject
    func createVehicleAt(_ pos: SCNVector3) -> RIDPhysicsObject
    func createTrackSegmentAt(_ origin: SCNVector3, rotation: SCNVector4, vertices: [SCNVector3]) -> RIDPhysicsObject
}

protocol RIDGraphicsUniverse {
    func createBall(_ radius: Float) -> RIDGraphicsObject
    func createFloor() -> RIDGraphicsObject
    func createVehicle() -> RIDGraphicsObject
    func createTrackSegment(_ segmentId: Int, vertices: [(left: SCNVector3, center: SCNVector3, right: SCNVector3)]) -> RIDGraphicsObject
}

class RIDGameObject {
    let body: RIDPhysicsObject
    let node: RIDGraphicsObject
    
    init(body: RIDPhysicsObject, node: RIDGraphicsObject) {
        self.body = body
        self.node = node
    }
    
    func update() {
        body.update()
        node.update(body)
    }
}

class RIDVehicle : RIDGameObject {
    override init(body: RIDPhysicsObject, node: RIDGraphicsObject) {
        super.init(body: body, node: node)
        _log.v("\(body)")
    }
}

class RIDUniverse {
    let _physics: RIDPhysicsUniverse
    let _graphics: RIDGraphicsUniverse
    var _gameObjects = [RIDGameObject]()
    var segmentId = 0 // FIXME not here

    init(physics: RIDPhysicsUniverse, graphics: RIDGraphicsUniverse) {
        self._physics = physics
        self._graphics = graphics
    }
    
    func step(_ deltaTime: Float = 1.0/60.0) {
        _physics.step(deltaTime)
        _gameObjects.forEach{$0.update()}
    }

    func createBall(_ radius: Float = 1.0) {
        createObject(_physics.createBall(radius), _graphics.createBall(radius))
    }
    
    func createFloor(_ y: Float) {
        createObject(_physics.createFloor(y), _graphics.createFloor())
    }

    func createVehicleAt(_ pos: SCNVector3) {
        _gameObjects.append(RIDVehicle(
            body: _physics.createVehicleAt(pos),
            node: _graphics.createVehicle()))
    }

    func createTrackSegmentAt(_ segment: RIDTrackSegment, renderer: inout RIDTrackRenderer) {
        let coords = renderer.renderSegment(segment)
//        for coord in coords {
//            trilog.i("\(coord)")
//        }

        createObject(
            _physics.createTrackSegmentAt(
                SCNVector3(0, 0, 0),
                rotation: SCNVector4(0, 0, 0, 0),
                vertices: coords.flatMap{[$0.left, $0.right]}),
            _graphics.createTrackSegment(segmentId, vertices: coords))
        
        segmentId += 1
    }

    fileprivate func createObject(_ body: RIDPhysicsObject, _ node: RIDGraphicsObject) {
        _gameObjects.append(RIDGameObject(body: body, node: node))
    }

    fileprivate func createObject(_ bodies: [RIDPhysicsObject], _ nodes: [RIDGraphicsObject]) {
        precondition(
            bodies.count == nodes.count,
            "Arrays of different lengths: bodies.count=\(bodies.count) nodes.count=\(nodes.count)")

        for pair in zip(bodies, nodes) {
            _gameObjects.append(RIDGameObject(body: pair.0, node: pair.1))
        }
    }
}
