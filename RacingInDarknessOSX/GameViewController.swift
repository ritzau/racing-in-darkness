//
//  GameViewController.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/7/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit
import SpriteKit

class GameViewController: NSViewController {
    
    @IBOutlet fileprivate weak var gameView: GameView!

    override func awakeFromNib() {
        launchRacingInDarknessInGameView(gameView, hasTouch: false)
    }
}
