//
//  GameView.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/7/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

class GameView: SCNView {

    fileprivate static let _gameKeyMap: [UInt16: RIDGameInputKey] = [
        0:  .steerLeft,  // A
        1:  .break,      // S
        2:  .steerRight, // D
        13: .throttle,   // W
        15: .reset,      // R
    ]

    fileprivate static let _menuKeyMap: [UInt16: RIDMenuInputKey] = [
        123: .back,
        124: .enter,
        125: .down,
        126: .up,
    ]

    internal var _overlay: RIDButtonOverlay?
    internal var _inputHandler: RIDInputHandler?

    fileprivate var _mode: RIDInputMode {
        return (_inputHandler?.inputMode ?? .none)!
    }

    fileprivate func buttonAtPoint(_ point: CGPoint) -> RIDGameInputKey? {
        let p = self.convert(point, from: nil)
        return _overlay?.buttonAtPoint(p)
    }

    override func mouseDown(with event: NSEvent) {
        guard _mode == .game,
              let button = buttonAtPoint(event.locationInWindow)
        else {
            super.mouseDown(with: event)
            return
        }

        _inputHandler?.gameInputController?.pressKey(button)
    }

    override func mouseUp(with event: NSEvent) {
        guard _mode == .game,
            let button = buttonAtPoint(event.locationInWindow)
        else {
            super.mouseUp(with: event)
            return
        }

        _inputHandler?.gameInputController?.releaseKey(button)
    }
    
    override func keyDown(with theEvent: NSEvent) {
        switch (_mode) {
        case .menu:
            if let key = GameView._menuKeyMap[theEvent.keyCode] {
                _inputHandler?.menuInputController.pressKey(key)
                return
            }
            break

        case .game:
            if let key = GameView._gameKeyMap[theEvent.keyCode] {
                _inputHandler?.gameInputController?.pressKey(key)
                return
            }
            break

        case .none:
            break
        }

        Swift.print("Unhandeld key=\(theEvent.keyCode)")
    }

    override func keyUp(with theEvent: NSEvent) {
        guard _mode == .game,
              let key = GameView._gameKeyMap[theEvent.keyCode]
        else {
            return
        }

        _inputHandler?.gameInputController?.releaseKey(key)
    }
}
