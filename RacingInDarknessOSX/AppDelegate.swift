//
//  AppDelegate.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/7/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var window: NSWindow!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
}
