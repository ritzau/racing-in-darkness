//
//  GameViewController.swift
//  Racing in Darkness tvOS
//
//  Created by Tobias Ritzau on 11/28/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import SpriteKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let gameView = view as? GameView else {
            preconditionFailure()
        }

        launchRacingInDarknessInGameView(gameView, hasTouch: false)
    }

    override func pressesBegan(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
//        _player.controls.pressThrottleKey()
    }

    override func pressesEnded(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
//        _player.controls.releaseThrottleKey()
    }

    override func pressesCancelled(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
//        _player.controls.releaseThrottleKey()
    }

    func handleTap(gestureRecognize: UIGestureRecognizer) {
//        // retrieve the SCNView
//        let scnView = self.view as! SCNView
//
//        // check what nodes are tapped
//        let p = gestureRecognize.locationInView(scnView)
//        let hitResults = scnView.hitTest(p, options: nil)
//        // check that we clicked on at least one object
//        if hitResults.count > 0 {
//            // retrieved the first clicked object
//            let result: AnyObject! = hitResults[0]
//            
//            // get its material
//            let material = result.node!.geometry!.firstMaterial!
//            
//            // highlight it
//            SCNTransaction.begin()
//            SCNTransaction.setAnimationDuration(0.5)
//            
//            // on completion - unhighlight
//            SCNTransaction.setCompletionBlock {
//                SCNTransaction.begin()
//                SCNTransaction.setAnimationDuration(0.5)
//                
//                material.emission.contents = UIColor.blackColor()
//                
//                SCNTransaction.commit()
//            }
//            
//            material.emission.contents = UIColor.redColor()
//            
//            SCNTransaction.commit()
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

}
