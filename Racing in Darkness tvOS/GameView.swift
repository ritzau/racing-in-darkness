//
//  GameView.swift
//  RacingInDarknessOSX
//
//  Created by Tobias Ritzau on 10/7/15.
//  Copyright (c) 2015 Tobias Ritzau. All rights reserved.
//

import SceneKit

func ridClamp(value: Float, _ min: Float, _ max: Float) -> Float {
    if value < min {
        return min
    }

    if value > max {
        return max
    }

    return value
}

class GameView: SCNView {

    var _inputHandler: RIDInputHandler!
    var _overlay: RIDButtonOverlay!

    var _currentTouch: UITouch?
    var _touchDown = CGPoint()

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            _currentTouch = touch
            _touchDown = touch.locationInView(self)
            trilog.i("touchDown=\(_touchDown)")
        }
    }

    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            if touch === _currentTouch {
                let p = touch.locationInView(self)
                _inputHandler.gameInputController?.steering = ridClamp(Float(-(p.x - _touchDown.x)) / 500.0, -1, 1)
            }
        }
    }

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        _inputHandler.gameInputController?.steering = 0
    }
}
